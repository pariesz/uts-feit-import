
$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition
$filesCsv = join-path -path $scriptPath -childpath "files.csv"

if (-not(test-path $filesCsv)){
	Write-host "$filesCsv does not exist"
	exit
}

function Get-Directory([string]$prompt) {
	while ($dir -eq $null){
		$dir = read-host "$prompt"
		if (-not(test-path $dir)){
			Write-host "Invalid path, re-enter." -foreground "Red"
			$dir = $null

		} elseif (-not (get-item $dir).psiscontainer){
			Write-host "Must be a directory, re-enter." -foreground "Red"
			$dir = $null
		}
	}
	return $dir
}

$src = Get-Directory("Enter source uploads directory");
$dest = Get-Directory("Enter destination uploads directory");

Import-Csv $filesCsv | ForEach-Object {
	if($_.Src -eq $null) { throw "$filesCsv src column does not exist" }
	if($_.Src -eq "") { throw "$filesCsv src is empty" }
	$srcDir = "$src\$($_.Src)"
	if(-not(test-path $srcDir)) { return }
	
	if($_.Dest -eq $null) { throw "$filesCsv dest column does not exist" }
	if($_.Dest -eq "") { throw "$filesCsv dest is empty" }
	$destDir = "$dest\$($_.Dest)"

	Get-ChildItem $srcDir | ForEach-Object {
		$destFile = "$destDir\$($_.Name)"

		if(test-path $destFile) { 
			if (-not (get-item $destFile).psiscontainer){
				Write-host "$destFile already exists" -foreground "Yellow"
				return
			}
		} 
		
		if(-not(test-path $destDir)) { 
			new-item -Path $destDir -ItemType directory | Out-Null
		}

		Write-host $destFile -foreground "Green"
		Copy-Item $_.FullName $destDir -Recurse -Force
	}
}
