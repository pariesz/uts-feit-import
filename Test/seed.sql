begin transaction

insert [DataImportTest].[dbo].[Categories](
      [ParentId]
      ,[Name]
      ,[Index]
      ,[LeftBound]
      ,[RightBound]
      ,[IsActive]
      ,[IsGeneral]
)
select
	ck_in.CategoryId
	,[Name]
    ,[Index]
    ,[LeftBound]
    ,[RightBound]
    ,[IsActive]
    ,[IsGeneral]
FROM [CareerHub].[dbo].[Categories] c
inner join [CareerHub].[dbo].CategoryKeys ck on ck.CategoryId = c.ParentId
inner join [DataImportTest].[dbo].CategoryKeys ck_in on ck_in.[Key] = ck.[Key]
where not exists (
	select null from [DataImportTest].[dbo].[Categories]
	where Name = c.Name and ParentId = ck_in.CategoryId
)
go

insert [DataImportTest].[dbo].[Email_System_Addresses] (
    [Type]
    ,[Email]
    ,[DisplayName]
    ,[Signature]
    ,[Archived]
)
SELECT [Type]
    ,[Email]
    ,[DisplayName]
    ,[Signature]
    ,[Archived]
FROM [CareerHub].[dbo].[Email_System_Addresses]
WHERE [Type] IN (0,1)
go

set IDENTITY_INSERT [DataImportTest].[dbo].[ContactDetails] OFF
set IDENTITY_INSERT [DataImportTest].[dbo].[JobSeeker_EmailAddresses] OFF
set IDENTITY_INSERT [DataImportTest].[dbo].Users OFF
set IDENTITY_INSERT [DataImportTest].[dbo].[Entities] OFF
go
set IDENTITY_INSERT [DataImportTest].[dbo].[ContactDetails] ON
go
INSERT [DataImportTest].[dbo].[ContactDetails] (
	[Id]
    ,[Address]
    ,[Suburb]
    ,[City]
    ,[State]
    ,[Postcode]
    ,[Country]
    ,[Phone]
    ,[Mobile]
    ,[Fax]
    ,[Url]
)
SELECT [Id]
      ,[Address]
      ,[Suburb]
      ,[City]
      ,[State]
      ,[Postcode]
      ,[Country]
      ,[Phone]
      ,[Mobile]
      ,[Fax]
      ,[Url]
  FROM CareerHub.[dbo].[ContactDetails] cd
INNER JOIN [CareerHub].[dbo].JobSeekers js ON js.ContactDetailsId = cd.Id
go
set IDENTITY_INSERT [DataImportTest].[dbo].[ContactDetails] OFF
go
set IDENTITY_INSERT [DataImportTest].[dbo].[JobSeeker_EmailAddresses] ON
go
INSERT [DataImportTest].[dbo].[JobSeeker_EmailAddresses] (
	[ID]
	,[Email]
	,[Confirmed]
	,[ConfirmCode]
	,[Deactivated]
	,[DeactivationReason]
	,[TransientBounceCount]
)
SELECT [ID]
	,[Email]
	,[Confirmed]
	,[ConfirmCode]
	,[Deactivated]
	,[DeactivationReason]
	,[TransientBounceCount]
FROM [CareerHub].[dbo].[JobSeeker_EmailAddresses]
go
set IDENTITY_INSERT [DataImportTest].[dbo].[JobSeeker_EmailAddresses] OFF
go
set IDENTITY_INSERT [DataImportTest].[dbo].Users ON
go
INSERT DataImportTest.[dbo].Users (
	[Id]
      ,[UserName]
      ,[Password]
      ,[PasswordReminder]
      ,[PasswordEncryptType]
      ,[IsActive]
      ,[Created]
      ,[LastLogin]
      ,[OpenID]
      ,[UpdateCache]
      ,[RequirePasswordReset]
)
SELECT [Id]
      ,[UserName]
      ,[Password]
      ,[PasswordReminder]
      ,[PasswordEncryptType]
      ,[IsActive]
      ,[Created]
      ,[LastLogin]
      ,[OpenID]
      ,[UpdateCache]
      ,[RequirePasswordReset]
  FROM [CareerHub].[dbo].[Users] u
INNER JOIN [CareerHub].[dbo].JobSeekers js ON js.UserId = u.Id
go
set IDENTITY_INSERT [DataImportTest].[dbo].Users OFF
go
set IDENTITY_INSERT [DataImportTest].[dbo].[Entities] ON
go
INSERT DataImportTest.[dbo].[Entities] (
	[Id]
    ,[TypeName]
    ,[Added]
    ,[LastUpdated]
    ,[IsDistributed]
    ,[IsUnapproved]
    ,[IsDeleted]
    ,[IsSoftDeleted]
)
SELECT [Id]
      ,[TypeName]
      ,[Added]
      ,[LastUpdated]
      ,[IsDistributed]
      ,[IsUnapproved]
      ,[IsDeleted]
      ,[IsSoftDeleted]
  FROM [CareerHub].[dbo].[Entities]
 WHERE [TypeName] = 'JobSeeker'
go
set IDENTITY_INSERT [DataImportTest].[dbo].[Entities] OFF
go
INSERT DataImportTest.[dbo].[JobSeekers] (
	[EntityId]
      ,[UserId]
      ,[FirstName]
      ,[LastName]
      ,[ContactDetailsId]
      ,[ExternalId]
      ,[IsProvisioned]
      ,[CardID]
      ,[IsDeceased]
      ,[AcceptedTermsDate]
      ,[PrimaryEmailID]
      ,[BackupEmailID]
)
SELECT [EntityId]
      ,[UserId]
      ,[FirstName]
      ,[LastName]
      ,[ContactDetailsId]
      ,[ExternalId]
      ,[IsProvisioned]
      ,[CardID]
      ,[IsDeceased]
      ,[AcceptedTermsDate]
      ,[PrimaryEmailID]
      ,[BackupEmailID]
  FROM [CareerHub].[dbo].[JobSeekers]
go

commit transaction