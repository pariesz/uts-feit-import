set nocount on
SET XACT_ABORT ON

begin transaction

alter table DataImportTest.dbo.Entities add feit_id int null;
alter table DataImportTest.dbo.Users add feit_id int null;
alter table DataImportTest.dbo.ContactDetails add feit_id int null;
alter table DataImportTest.dbo.Administrator_EmailAddresses add feit_id int null;
alter table DataImportTest.dbo.Organisation_EmailAddresses add feit_id int null;
alter table DataImportTest.dbo.Individual_EmailAddresses add feit_id int null;
alter table DataImportTest.dbo.OrganisationContact_EmailAddresses add feit_id int null;
alter table DataImportTest.dbo.JobSeeker_EmailAddresses add feit_id int null;
alter table DataImportTest.dbo.SubSites add feit_id int null;
alter table DataImportTest.dbo.Blogs add feit_id int null;
alter table DataImportTest.dbo.Workgroups add feit_id int null;
alter table DataImportTest.dbo.StoredFiles add feit_id int null;
alter table DataImportTest.dbo.Uploads add feit_id int null;
alter table DataImportTest.dbo.Forms_Pages add feit_id int null;
alter table DataImportTest.dbo.Forms_Fields add feit_id int null;
alter table DataImportTest.dbo.FormReports add feit_id int null;
alter table DataImportTest.dbo.Forms_Submissions add feit_id int null;
alter table DataImportTest.dbo.QuestionResponses add feit_id int null;
alter table DataImportTest.dbo.Email_Batches add feit_id int null;
alter table DataImportTest.dbo.Email_Jobs add feit_id int null;
alter table DataImportTest.dbo.Email_Mailouts add feit_id int null;
go

CREATE UNIQUE NONCLUSTERED INDEX ux_Entities_feit_id ON DataImportTest.dbo.Entities(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_Users_feit_id ON DataImportTest.dbo.Users(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_ContactDetails_feit_id ON DataImportTest.dbo.ContactDetails(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_Administrator_EmailAddresses_feit_id ON DataImportTest.dbo.Administrator_EmailAddresses(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_Organisation_EmailAddresses_feit_id ON DataImportTest.dbo.Organisation_EmailAddresses(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_Individual_EmailAddresses_feit_id ON DataImportTest.dbo.Individual_EmailAddresses(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_OrganisationContact_EmailAddresses_feit_id ON DataImportTest.dbo.OrganisationContact_EmailAddresses(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_JobSeeker_EmailAddresses_feit_id ON DataImportTest.dbo.JobSeeker_EmailAddresses(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_SubSites_feit_id ON DataImportTest.dbo.SubSites(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_Blogs_feit_id ON DataImportTest.dbo.Blogs(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_Workgroups_feit_id ON DataImportTest.dbo.Workgroups(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_StoredFiles_feit_id ON DataImportTest.dbo.StoredFiles(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_Uploads_feit_id ON DataImportTest.dbo.Uploads(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_Forms_Pages_feit_id ON DataImportTest.dbo.Forms_Pages(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_Forms_Fields_feit_id ON DataImportTest.dbo.Forms_Fields(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_FormReports_feit_id ON DataImportTest.dbo.FormReports(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_Forms_Submissions_feit_id ON DataImportTest.dbo.Forms_Submissions(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_QuestionResponses_feit_id ON DataImportTest.dbo.QuestionResponses(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_Email_Batches_feit_id ON DataImportTest.dbo.Email_Batches(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_Email_Jobs_feit_id ON DataImportTest.dbo.Email_Jobs(feit_id) WHERE feit_id IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX ux_Email_Mailouts_feit_id ON DataImportTest.dbo.Email_Mailouts(feit_id) WHERE feit_id IS NOT NULL;
go


-- fix categories
DECLARE @Merge TABLE (
	k nvarchar(1000) Collate Latin1_General_CI_AS,
	f nvarchar(1000) Collate Latin1_General_CI_AS,
	t nvarchar(1000) Collate Latin1_General_CI_AS
)
INSERT INTO @Merge (k, f, t) VALUES 
('TypeOfWork', 'IT', 'Internship'),
('TypeOfWork', 'Junior Engineering', 'Internship'),
('TypeOfWork', 'Senior Engineering', 'Internship'),
('TypeOfWork', 'Graduate Recruitment Programme', 'Internship'),
('TypeOfWork', 'Vacation Employment Programme', 'Internship'),
('Service', 'IT', 'Internships'),
('Service', 'Junior Engineering', 'Internships'),
('Service', 'Senior Engineering', 'Internships'),
('Occupation', 'Engineering - General', 'Engineering'),
('Occupation', 'Engineering - Biomedical', 'Engineering'),
('Occupation', 'Engineering - Civil', 'Engineering'),
('Occupation', 'Engineering - Civil & Environmental', 'Engineering'),
('Occupation', 'Engineering - Mechanical', 'Engineering'),
('Occupation', 'Engineering - Mechanical & Mechatronic', 'Engineering'),
('Occupation', 'Engineering - Mechatronic', 'Engineering'),
('Occupation', 'Engineering - Electrical', 'Engineering'),
('Occupation', 'Engineering - Enviromental', 'Engineering'),
('Occupation', 'Engineering - ICT (Software)', 'Engineering'),
('Occupation', 'Engineering - ICT (Computer Systems)', 'Engineering'),
('Occupation', 'Engineering - ICT (Telecommunications)', 'Engineering'),
('Occupation', 'Engineering - Innovation', 'Engineering'),
('Occupation', 'IT', 'Engineering'),
('Discipline', 'Engineering - General', 'Engineering'),
('Discipline', 'Engineering - Biomedical', 'Engineering'),
('Discipline', 'Engineering - Civil', 'Engineering'),
('Discipline', 'Engineering - Civil & Environmental', 'Engineering'),
('Discipline', 'Engineering - Mechanical', 'Engineering'),
('Discipline', 'Engineering - Mechanical & Mechatronic', 'Engineering'),
('Discipline', 'Engineering - Electrical', 'Engineering'),
('Discipline', 'Engineering - ICT (Software)', 'Engineering'),
('Discipline', 'Engineering - ICT (Computer Systems)', 'Engineering'),
('Discipline', 'Engineering - ICT (Telecommunications)', 'Engineering'),
('Discipline', 'Engineering - Innovation', 'Engineering'),
('Discipline', 'IT', 'Engineering')
--('Industry', 'ICT', '')
--('Industry', 'IT', '')
--('Industry', 'Innovation', '')
--('Industry', 'Civil', '')
--('Industry', 'Civil & Environmental', '')
--('Industry', 'Electrical', '')
--('Industry', 'Mechanical', '')
--('Industry', 'Mechanical and Mechatronic', '')


declare @keep int,  @del int, @k nvarchar(100), @f nvarchar(100), @t nvarchar(100);
WHILE EXISTS(SELECT NULL FROM @Merge) BEGIN
	SELECT TOP 1 @f = f, @t = t, @k = k, @keep = c_to.Id, @del = c_from.Id
	FROM @Merge
	INNER JOIN CareerHub.dbo.CategoryKeys ck ON ck.[Key] = k
	LEFT JOIN CareerHub.dbo.Categories c_from ON c_from.parentid = ck.categoryId and c_from.name = f
	LEFT JOIN CareerHub.dbo.Categories c_to ON c_to.parentid = ck.categoryId and c_to.name = t

	DELETE TOP(1) FROM @Merge
		
	IF @del IS NULL BEGIN	
		RAISERROR('%s "%s" does not exist', 16, 1, @k, @f);

	END ELSE IF @keep IS NULL BEGIN
		RAISERROR('Renaming %s "%s" to "%s"', 10, 1, @k, @f, @t);
		UPDATE CareerHub.dbo.Categories SET Name = @t WHERE Id = @del

	END ELSE if @keep = @del BEGIN
		RAISERROR('Cannot merge %s "%s" into itself!', 16, 1, @k, @f);
	
	END ELSE BEGIN
		RAISERROR('Merging %s "%s" (%d) to "%s" (%d)', 10, 1, @k, @f, @del, @t, @keep)
	
		update CareerHub.dbo.AppliedCategories
		set CategoryId = @keep
		where CategoryId = @del
		and EntityId NOT IN (SELECT EntityId FROM CareerHub.dbo.AppliedCategories WHERE CategoryId = @keep)

		update CareerHub.dbo.appointmentlocations
		set campusid = @keep from CareerHub.dbo.appointmentlocations ac
		where ac.CampusId = @del

		update CareerHub.dbo.WorkflowComponents
		set CategoryID = @keep
		where CategoryID = @del

		update CareerHub.dbo.Portfolios_Categories
		set CategoryID = @keep
		where CategoryID = @del
		and PortfolioID not in (select PortfolioID from CareerHub.dbo.Portfolios_Categories where CategoryID = @keep)
	
		update CareerHub.dbo.WebIDCategoryMappings
		set MappedCategoryID = @keep
		where MappedCategoryID = @del

		update CareerHub.dbo.WebIDCategoryMappings
		set TempCategoryID = null, MappedCategoryID = @keep, Processed = GETUTCDATE()
		where TempCategoryID = @del

		update CareerHub.dbo.DistributorCategories_Mappings
		set LocalId = @keep
		where LocalId = @del
		and RemoteId not in (select RemoteId from CareerHub.dbo.DistributorCategories_Mappings where LocalId = @keep)

		update CareerHub.dbo.FilterCategories
		set CategoryID = @keep
		where CategoryID = @del
		and FilterID not in (select FilterID from CareerHub.dbo.FilterCategories where CategoryID = @keep)
	
		delete from CareerHub.dbo.DistributorCategories_Mappings WHERE LocalId = @del
		delete from CareerHub.dbo.AppliedCategories WHERE CategoryId = @del
		delete from CareerHub.dbo.Portfolios_Categories WHERE CategoryId = @del
		delete from CareerHub.dbo.FilterCategories where CategoryID = @del
		delete from CareerHub.dbo.Categories WHERE Id = @del
	END
END

;with catindexes as (
	select *, row_number() over (partition by parentId order by [index]) - 1 ind
	from CareerHub.dbo.categories
)

update catindexes set [index] = ind
exec CareerHub.dbo.Categories_UpdateBounds 1

-- users
insert DataImportTest.dbo.Users (
	feit_id,[UserName],
	[Password],[PasswordReminder],[PasswordEncryptType],[IsActive],[Created],[LastLogin],[OpenID],[UpdateCache],[RequirePasswordReset])
select 
	u.[Id], case when u_out.UserName is not null then u.[UserName] + '_FEIT' else u.[UserName] end,
	u.[Password],u.[PasswordReminder],u.[PasswordEncryptType],u.[IsActive],u.[Created],u.[LastLogin],u.[OpenID],u.[UpdateCache],u.[RequirePasswordReset]
from CareerHub.dbo.Users u
left join DataImportTest.dbo.Users u_out on u_out.username = u.username
where u.Id in (
	select UserId from CareerHub.dbo.Administrators
	union all select UserId from CareerHub.dbo.Individuals
	union all select UserId from CareerHub.dbo.OrganisationContacts
)
RAISERROR('%d Users', 10, 1, @@ROWCOUNT) WITH NOWAIT

declare @jsRoleId int
select @jsRoleId = Id FROM DataImportTest.dbo.Roles where name = 'JobSeeker'

insert DataImportTest.dbo.users_roles (UserId, RoleId)
select u.Id,r.Id
from CareerHub.dbo.users_roles x
inner join CareerHub.dbo.roles x_r on x.RoleId = x_r.Id
inner join DataImportTest.dbo.Roles r on r.Name = x_r.Name and (r.IsSystemRole = 1 or r.ParentId = @jsRoleId)
inner join DataImportTest.dbo.Users u on u.feit_id = x.UserId
RAISERROR('%d users_roles', 10, 1, @@ROWCOUNT) WITH NOWAIT

update u
set feit_id = x.UserId
from CareerHub.dbo.JobSeekers x
inner join DataImportTest.dbo.JobSeekers js on js.ExternalId = x.ExternalId and NULLIF(js.ExternalId, '') is not null
inner join DataImportTest.dbo.Users u on u.Id = js.UserId
where u.feit_id is null and not exists (select null from DataImportTest.dbo.Users u_sub where u_sub.feit_id = x.UserId)
RAISERROR('%d JobSeeker Users feit_id Updated', 10, 1, @@ROWCOUNT) WITH NOWAIT

-- entities
insert DataImportTest.dbo.Entities (
	feit_id,[AddedUserId],[LastUpdatedUserId],
	[TypeName],[Added],[LastUpdated],[IsDistributed],[IsUnapproved],[IsDeleted],[IsSoftDeleted])
select 
	e.[Id],u1.Id,u2.Id,
	[TypeName],[Added],[LastUpdated],[IsDistributed],[IsUnapproved],[IsDeleted],[IsSoftDeleted]
from CareerHub.dbo.Entities e
left join DataImportTest.dbo.Users u1 on u1.feit_id = e.AddedUserId and e.AddedUserId is not null
left join DataImportTest.dbo.Users u2 on u2.feit_id = e.[LastUpdatedUserId] and e.[LastUpdatedUserId] is not null
where e.TypeName in ('Administrator','Form','Individual','Job','JournalEntry','Organisation','OrganisationContact','Question')
or e.Id in (
	select js.entityId 
	from CareerHub.dbo.JobSeekers js
	inner join CareerHub.dbo.Administrators a on js.UserId = a.UserId
)
RAISERROR('%d Entities', 10, 1, @@ROWCOUNT) WITH NOWAIT


-- contact details
insert DataImportTest.dbo.[ContactDetails]
(feit_id	,[Address],[Suburb],[City],[State],[Postcode],[Country],[Phone],[Mobile],[Fax],[Url])
select [Id]	,[Address],[Suburb],[City],[State],[Postcode],[Country],[Phone],[Mobile],[Fax],[Url]
from CareerHub.dbo.[ContactDetails] cd
where cd.Id in (
	select ContactDetailsId from CareerHub.dbo.Administrators
	union all select ContactDetailsId from CareerHub.dbo.Employers
	union all select StreetContactDetailsId from CareerHub.dbo.Organisations
	union all select ContactDetailsId from CareerHub.dbo.OrganisationContacts
	union all select js.ContactDetailsId from CareerHub.dbo.JobSeekers js inner join CareerHub.dbo.Administrators a on js.UserId = a.UserId
)
RAISERROR('%d ContactDetails', 10, 1, @@ROWCOUNT) WITH NOWAIT


-- entities users
insert DataImportTest.dbo.Entities_Users (EntityId,UserId,ContactDetailsId)
select e.id,u.id,cd.id
from CareerHub.dbo.Entities_Users x
inner join  DataImportTest.dbo.Entities e on e.feit_id = x.EntityId
inner join  DataImportTest.dbo.Users u on u.feit_id = x.UserId
inner join  DataImportTest.dbo.ContactDetails cd on cd.feit_id = x.ContactDetailsId
RAISERROR('%d Entities_Users', 10, 1, @@ROWCOUNT) WITH NOWAIT


update e
set feit_id = x.EntityId
from CareerHub.dbo.JobSeekers x
inner join DataImportTest.dbo.JobSeekers js on js.ExternalId = x.ExternalId and NULLIF(js.ExternalId, '') is not null
inner join DataImportTest.dbo.Entities e on e.Id = js.EntityId
where feit_id is null and not exists (select null from DataImportTest.dbo.Entities e_sub where e_sub.feit_id = x.EntityId)
RAISERROR('%d JobSeeker Entities feit_id Updated ', 10, 1, @@ROWCOUNT) WITH NOWAIT


-- applied categories
insert DataImportTest.dbo.[AppliedCategories]([EntityId],[CategoryId],[CategoryKey])
select e.Id,c_in.Id,x.[CategoryKey]
FROM CareerHub.dbo.[AppliedCategories] x
inner join DataImportTest.dbo.Entities e on e.feit_id = x.EntityId
inner join CareerHub.dbo.Categories c_out on c_out.Id = x.CategoryId
inner join DataImportTest.dbo.Categories c_in on c_in.Name = c_out.name
inner join DataImportTest.dbo.CategoryKeys ck on ck.[Key] = x.CategoryKey and ck.CategoryId = c_in.ParentId
where not exists (
	select null from DataImportTest.dbo.[AppliedCategories] ac
	where ac.EntityId = e.Id and ac.CategoryId = c_in.Id
)
RAISERROR('%d AppliedCategories', 10, 1, @@ROWCOUNT) WITH NOWAIT


-- StoredFiles
insert DataImportTest.dbo.StoredFiles
(feit_id	,[FileName],[MimeType],[Bytes],[FileSize],[Modified])
select [Id]	,[FileName],[MimeType],[Bytes],[FileSize],[Modified]
from CareerHub.dbo.StoredFiles
RAISERROR('%d StoredFiles', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.Attachments ([EntityId],[StoredFileId],[Title])
select e.Id, sf.Id, Title
from CareerHub.dbo.Attachments x
inner join DataImportTest.dbo.StoredFiles sf on sf.feit_id = x.StoredFileId
inner join DataImportTest.dbo.Entities e on e.feit_id = x.EntityId
RAISERROR('%d Attachments', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.[Uploads]
(feit_id,[UserID],[RelativePath],[Added],[ContentType],[Filename],[Filesize])
select x.[ID],u.Id,[RelativePath],[Added],[ContentType],[Filename],[Filesize]
FROM CareerHub.dbo.[Uploads] x
left join DataImportTest.dbo.Users u on u.feit_id = x.UserId and x.UserId is not null
RAISERROR('%d Uploads', 10, 1, @@ROWCOUNT) WITH NOWAIT

-- SystemEmailAddresses
insert DataImportTest.dbo.[Email_System_Addresses] ([Type],[Email],[DisplayName],[Signature],[Archived])
select [Type],[Email],[DisplayName],[Signature],[Archived]
from CareerHub.dbo.[Email_System_Addresses]
where [type] = 2
and email not in (select Email from DataImportTest.dbo.[Email_System_Addresses] where [Type] = 2)
RAISERROR('%d Email_System_Addresses', 10, 1, @@ROWCOUNT) WITH NOWAIT


-- workgroups
insert DataImportTest.dbo.SubSites 
(feit_id	,Css,Settings)
select Id	,'~/Resources/Themes/Workgroups/UTS_FEIT/main.css',Settings
from CareerHub.dbo.SubSites
RAISERROR('%d SubSites', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.[Widgets] 
([SubSiteId]	,[View],[Area],[WidgetName],[Order],[Settings])
select ss.Id	,[View],[Area],[WidgetName],[Order],w.[Settings]
FROM CareerHub.dbo.[Widgets] w
inner join DataImportTest.dbo.SubSites ss on ss.feit_id = w.SubSiteId
RAISERROR('%d Widgets', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.Blogs 
(feit_id	,handle,title)
select Id	,handle,title
from CareerHub.dbo.Blogs
RAISERROR('%d Blogs', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert  DataImportTest.dbo.BlogPosts
([BlogId],[UserId],[Handle],[Title]		,[Summary],[Body],[Publish],[Author],[AllowComments])
select b.Id,u.Id,bc.[Handle],bc.[Title]	,[Summary],[Body],[Publish],[Author],[AllowComments]
from CareerHub.dbo.BlogPosts bc
inner join DataImportTest.dbo.Blogs b on b.feit_id = bc.BlogId
inner join DataImportTest.dbo.Users u on u.feit_id = bc.UserId
RAISERROR('%d BlogPosts', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.WorkGroups 
(feit_id	,[Name],[Handle],[SharePortfoliosWith],[ManageCredits],[BlogId],[SubSiteId])
select wg.[Id],wg.[Name],wg.[Handle],wg.[SharePortfoliosWith],wg.[ManageCredits],b.Id,ss.id
from CareerHub.dbo.WorkGroups wg
left join DataImportTest.dbo.Blogs b on b.feit_id = wg.BlogId and wg.BlogId is not null
left join DataImportTest.dbo.SubSites ss on ss.feit_id = wg.SubSiteId and wg.SubSiteId is not null
RAISERROR('%d WorkGroups', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.WorkGroupLinkedEntities (WorkGroupId, EntityId, Added)
select wg.Id, e.Id, wgle.Added
from CareerHub.dbo.WorkGroupLinkedEntities wgle
inner join DataImportTest.dbo.WorkGroups wg on wg.feit_id = wgle.WorkGroupId
inner join DataImportTest.dbo.Entities e on e.feit_id = wgle.EntityId
RAISERROR('%d WorkGroupLinkedEntities', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.Users_WorkGroups (WorkGroupId, UserId)
select wg.Id, u.Id
from CareerHub.dbo.Users_WorkGroups uwg
inner join DataImportTest.dbo.WorkGroups wg on wg.feit_id = uwg.WorkGroupId
inner join DataImportTest.dbo.Users u on u.feit_id = uwg.UserId
RAISERROR('%d Users_WorkGroups', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.SecureEntities (EntityId, InheritFromParent, WorkGroupId)
select e.Id, InheritFromParent, wg.Id
from CareerHub.dbo.SecureEntities se
left join DataImportTest.dbo.WorkGroups wg on wg.feit_id = se.WorkGroupId
inner join DataImportTest.dbo.Entities e on e.feit_id = se.EntityId
where se.ParentId is null
RAISERROR('%d Parent SecureEntities', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.SecureEntities (EntityId, ParentId, InheritFromParent, WorkGroupId)
select e.Id, e_parent.Id, se.InheritFromParent, wg.Id
from CareerHub.dbo.SecureEntities se
left join DataImportTest.dbo.WorkGroups wg on wg.feit_id = se.WorkGroupId
inner join DataImportTest.dbo.Entities e on e.feit_id = se.EntityId
inner join DataImportTest.dbo.Entities e_parent on e_parent.feit_id = se.ParentId
inner join DataImportTest.dbo.SecureEntities se_parent on se_parent.EntityId = e_parent.Id
RAISERROR('%d Child SecureEntities', 10, 1, @@ROWCOUNT) WITH NOWAIT


-- labels
insert DataImportTest.dbo.[Entities_LabelDefinitions] (
	[LastEditedByUserId],[CreatedByUserId],[WorkGroupId],
	[Type],[Label],[Created]	,[LastEdited],[IsLocked],[IsPrivate],[IsArchived])
select 
	u_edited.Id, u_created.Id, wg.Id,
	[Type],[Label],eld.[Created],[LastEdited],[IsLocked],[IsPrivate],[IsArchived]
from CareerHub.dbo.[Entities_LabelDefinitions] eld
left join DataImportTest.dbo.WorkGroups wg on wg.feit_id = eld.[WorkGroupId] AND eld.[WorkGroupId] IS NOT NULL
left join DataImportTest.dbo.Users u_created on u_created.feit_id = eld.[CreatedByUserId] AND eld.[CreatedByUserId] IS NOT NULL
left join DataImportTest.dbo.Users u_edited on u_edited.feit_id = eld.[LastEditedByUserId] AND eld.[LastEditedByUserId] IS NOT NULL
where not exists (
	select null 
	from DataImportTest.dbo.[Entities_LabelDefinitions] existing
	where existing.[Type] = eld.[Type]
	and existing.Label = eld.Label
)
RAISERROR('%d Entities_LabelDefinitions', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.[Entities_Labels] (EntityId, LabelId)
select e.Id, eld_in.Id
from CareerHub.dbo.[Entities_Labels] el
inner join CareerHub.dbo.[Entities_LabelDefinitions] eld_out on el.LabelId = eld_out.Id
inner join DataImportTest.dbo.[Entities_LabelDefinitions] eld_in on eld_in.[Type] = eld_out.[Type] and eld_in.Label = eld_out.Label
inner join DataImportTest.dbo.Entities e on e.feit_id = el.EntityId
RAISERROR('%d Entities_Labels', 10, 1, @@ROWCOUNT) WITH NOWAIT


-- associations
insert DataImportTest.dbo.AssociationLabels (LabelA, LabelB)
select LabelA, LabelB
from CareerHub.dbo.AssociationLabels al
where not exists (
	select null 
	from DataImportTest.dbo.AssociationLabels existing
	where existing.LabelA = al.LabelA
	and existing.LabelB = al.LabelB
)
RAISERROR('%d AssociationLabels', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.AssociationRanks (EntityTypeA, EntityTypeB,AssociationLabelId,[Rank])
select EntityTypeA,EntityTypeB,al_in.Id,[Rank]
from CareerHub.dbo.AssociationRanks ar
inner join CareerHub.dbo.AssociationLabels al_out on ar.AssociationLabelId = al_out.Id
inner join DataImportTest.dbo.AssociationLabels al_in on al_in.LabelA = al_out.LabelA and al_in.LabelB = al_out.LabelB
RAISERROR('%d AssociationRanks', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.Associations (EntityIdA,EntityIdB,AssociationLabelId,Added,AddedUserId)
select ea.Id,eb.Id,al_in.Id,a.Added,u.Id
from CareerHub.dbo.Associations a
inner join CareerHub.dbo.AssociationLabels al_out on a.AssociationLabelId = al_out.Id
inner join DataImportTest.dbo.AssociationLabels al_in on al_in.LabelA = al_out.LabelA and al_in.LabelB = al_out.LabelB
inner join DataImportTest.dbo.Entities ea on ea.feit_id = a.EntityIdA
inner join DataImportTest.dbo.Entities eb on eb.feit_id = a.EntityIdB
left join DataImportTest.dbo.Users u on u.feit_id = a.AddedUserId and a.AddedUserId is not null
RAISERROR('%d Associations', 10, 1, @@ROWCOUNT) WITH NOWAIT


-- Journal Entries
insert DataImportTest.dbo.JournalEntries ([EntityId],[AttachedToEntityId],[IsPrivate],[Text],[Share])
select e.Id, e_attached.Id,[IsPrivate],[Text],[Share]
from CareerHub.dbo.JournalEntries je
inner join DataImportTest.dbo.Entities e on e.feit_id = je.EntityId
inner join DataImportTest.dbo.Entities e_attached on e_attached.feit_id = je.AttachedToEntityId
RAISERROR('%d JournalEntries', 10, 1, @@ROWCOUNT) WITH NOWAIT


-- emails
insert DataImportTest.dbo.[Administrator_EmailAddresses] (
	feit_id	,[Email],[Confirmed],[ConfirmCode],[Deactivated],[DeactivationReason],[TransientBounceCount])
SELECT [ID]	,[Email],[Confirmed],[ConfirmCode],[Deactivated],[DeactivationReason],[TransientBounceCount]
FROM CareerHub.dbo.[Administrator_EmailAddresses]
RAISERROR('%d Administrator_EmailAddresses', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.Organisation_EmailAddresses (
	feit_id	,[Email],[Confirmed],[ConfirmCode],[Deactivated],[DeactivationReason],[TransientBounceCount])
SELECT [ID]	,[Email],[Confirmed],[ConfirmCode],[Deactivated],[DeactivationReason],[TransientBounceCount]
FROM CareerHub.dbo.Organisation_EmailAddresses
RAISERROR('%d Organisation_EmailAddresses', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.OrganisationContact_EmailAddresses (
	feit_id	,[Email],[Confirmed],[ConfirmCode],[Deactivated],[DeactivationReason],[TransientBounceCount])
SELECT [ID]	,[Email],[Confirmed],[ConfirmCode],[Deactivated],[DeactivationReason],[TransientBounceCount]
FROM CareerHub.dbo.OrganisationContact_EmailAddresses
RAISERROR('%d OrganisationContact_EmailAddresses', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.Individual_EmailAddresses (
	feit_id	,[Email],[Confirmed],[ConfirmCode],[Deactivated],[DeactivationReason],[TransientBounceCount])
SELECT [ID]	,[Email],[Confirmed],[ConfirmCode],[Deactivated],[DeactivationReason],[TransientBounceCount]
FROM CareerHub.dbo.Individual_EmailAddresses
RAISERROR('%d Individual_EmailAddresses', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.JobSeeker_EmailAddresses (
	feit_id	,[Email],[Confirmed],[ConfirmCode],[Deactivated],[DeactivationReason],[TransientBounceCount])
SELECT [ID]	,[Email],[Confirmed],[ConfirmCode],[Deactivated],[DeactivationReason],[TransientBounceCount]
FROM CareerHub.dbo.JobSeeker_EmailAddresses x
INNER JOIN CareerHub.dbo.JobSeekers js ON js.BackupEmailID = x.ID or js.PrimaryEmailID = x.ID
INNER JOIN  CareerHub.dbo.Administrators a on a.UserId = js.UserId
RAISERROR('%d JobSeeker_EmailAddresses', 10, 1, @@ROWCOUNT) WITH NOWAIT


-- administrators
insert DataImportTest.dbo.Administrators (
	[EntityId],[UserId],[ContactDetailsId],[EmailID],[DefaultWorkGroupID],
	[Location],[NotifyApprovals],[Position],[ProfileRoles],[ShowContactDetails],[Profile],[EmailSignature],[FirstName],[LastName],[KeyContact],[ITContact])
select
	e.Id,u.Id,cd.Id,email.Id,wg.Id,
	[Location],[NotifyApprovals],[Position],[ProfileRoles],[ShowContactDetails],[Profile],[EmailSignature],[FirstName],[LastName],[KeyContact],[ITContact]
FROM CareerHub.dbo.[Administrators] a
inner join DataImportTest.dbo.Entities e on e.feit_id = a.[EntityId]
inner join DataImportTest.dbo.Users u on u.feit_id = a.[UserId]
inner join DataImportTest.dbo.Administrator_EmailAddresses email on email.feit_id = a.[EmailID]
inner join DataImportTest.dbo.ContactDetails cd on cd.feit_id = a.[ContactDetailsId]
left join  DataImportTest.dbo.WorkGroups wg on wg.feit_id = a.[DefaultWorkGroupID] and a.[DefaultWorkGroupID] is not null
RAISERROR('%d Administrators', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.Administrator_UpdateNotificationTypes (AdministratorID, [Type], EmailFrequency)
select e.Id, [Type], EmailFrequency
from CareerHub.dbo.Administrator_UpdateNotificationTypes x
inner join DataImportTest.dbo.Entities e on e.feit_id = x.AdministratorID
RAISERROR('%d Administrator_UpdateNotificationTypes', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.Administrator_DisabledUpdateNotificationFeatures (AdministratorID, Feature)
select e.Id, Feature
from CareerHub.dbo.Administrator_DisabledUpdateNotificationFeatures x
inner join DataImportTest.dbo.Entities e on e.feit_id = x.AdministratorID
RAISERROR('%d Administrator_DisabledUpdateNotificationFeatures', 10, 1, @@ROWCOUNT) WITH NOWAIT


-- approvals
insert DataImportTest.dbo.[Approvals] (
	[EntityId],[CheckedByAdminId],[LockedByAdminId],
	[DateChecked],[IsChecked],[IsApproved])
SELECT 
	e.Id,a_checked.Id,a_locked.Id,
	[DateChecked],[IsChecked],[IsApproved]
FROM CareerHub.dbo.[Approvals] x
inner join DataImportTest.dbo.Entities e on e.feit_id = x.EntityId
left join DataImportTest.dbo.Entities a_checked on a_checked.feit_id = x.CheckedByAdminId and x.CheckedByAdminId is not null
left join DataImportTest.dbo.Entities a_locked on a_locked.feit_id = x.LockedByAdminId and x.LockedByAdminId is not null
where [EntityId] not in (
	select [EntityId] from DataImportTest.dbo.[Approvals]
)
RAISERROR('%d Approvals', 10, 1, @@ROWCOUNT) WITH NOWAIT


-- Administrator JobSeekers
insert DataImportTest.dbo.[JobSeekers] (
	[EntityId],[UserId],[BackupEmailID],[PrimaryEmailID],[ContactDetailsId],
	[FirstName],[LastName],[IsProvisioned],[IsDeceased],[AcceptedTermsDate])
SELECT 
	e.Id,u.Id,email_backup.Id,email_primary.Id,cd.id,
	x.[FirstName],x.[LastName],[IsProvisioned],[IsDeceased],[AcceptedTermsDate]
FROM CareerHub.dbo.[JobSeekers] x
inner join CareerHub.dbo.Administrators x_a on x.UserId = x_a.UserId
inner join DataImportTest.dbo.Entities e on e.feit_id = x.EntityId
inner join DataImportTest.dbo.Users u on u.feit_id = x.UserId
inner join DataImportTest.dbo.ContactDetails cd on cd.feit_id = x.[ContactDetailsId]
inner join DataImportTest.dbo.JobSeeker_EmailAddresses email_backup on email_backup.feit_id = x.[BackupEmailID]
inner join DataImportTest.dbo.JobSeeker_EmailAddresses email_primary on email_primary.feit_id = x.[PrimaryEmailID]
RAISERROR('%d JobSeekers', 10, 1, @@ROWCOUNT) WITH NOWAIT


-- employers
insert DataImportTest.dbo.[Employers] ([EntityId] ,[ContactDetailsId] ,[EmployerType] ,[IsActive], [Description])
select e.Id, cd.Id, [EmployerType] ,[IsActive], [Description] 
from CareerHub.dbo.[Employers] emp
inner join DataImportTest.dbo.Entities e on e.feit_id = emp.EntityId
inner join DataImportTest.dbo.ContactDetails cd on cd.feit_id = emp.ContactDetailsId
RAISERROR('%d Employers', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.Individuals ([EntityId],[UserId],[EmailID],[FirstName],[LastName],[Title])
select e.Id,u.Id,email.Id,[FirstName],[LastName],[Title]
from CareerHub.dbo.[Individuals] i
inner join DataImportTest.dbo.Entities e on e.feit_id = i.EntityId
inner join DataImportTest.dbo.Individual_EmailAddresses email on email.feit_id = i.EmailID
left join DataImportTest.dbo.Users u on u.feit_id = i.UserId AND i.UserId IS NOT NULL
RAISERROR('%d Individuals', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.[Organisations] (
	[EntityId],[BannerFileId],[StreetContactDetailsId],[EmailID],
	[Name],[Division],[BusinessId],[IsPrivate],[Acronym])
select e.Id,sf.id,cd.Id,email.id,
	[Name],[Division],[BusinessId],[IsPrivate],[Acronym]
from CareerHub.dbo.[Organisations] x
inner join DataImportTest.dbo.Entities e on e.feit_id = x.EntityId
inner join DataImportTest.dbo.Organisation_EmailAddresses email on email.feit_id = x.EmailID
inner join DataImportTest.dbo.ContactDetails cd on cd.feit_id = x.StreetContactDetailsId
left join  DataImportTest.dbo.StoredFiles sf on sf.feit_id = x.BannerFileId AND x.BannerFileId IS NOT NULL
RAISERROR('%d Organisations', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.[OrganisationContacts] (
	[EntityId],[UserId],[EmailID],[OrganisationId],[ContactDetailsId],
	[Title],[FirstName],[LastName],[PositionTitle],[UseOrganisationAddress],[IsActive])
select e.Id,u.Id,email.Id,o.Id,cd.Id,
	[Title],[FirstName],[LastName],[PositionTitle],[UseOrganisationAddress],x.[IsActive]
FROM CareerHub.dbo.[OrganisationContacts] x
inner join DataImportTest.dbo.Entities e on e.feit_id = x.EntityId
inner join DataImportTest.dbo.OrganisationContact_EmailAddresses email on email.feit_id = x.EmailID
inner join DataImportTest.dbo.ContactDetails cd on cd.feit_id = x.ContactDetailsId
inner join DataImportTest.dbo.Entities o on o.feit_id = x.OrganisationId
left join DataImportTest.dbo.Users u on u.feit_id = x.UserId AND x.UserId IS NOT NULL
RAISERROR('%d OrganisationContacts', 10, 1, @@ROWCOUNT) WITH NOWAIT

update x
set [LocalContactId] = c_local.Id,
	[PrimaryContactId] = c_primary.Id
from DataImportTest.dbo.[Organisations] x
inner join DataImportTest.dbo.Entities o_in on o_in.id = x.EntityId
inner join CareerHub.dbo.[Organisations] o_out on o_out.EntityId = o_in.feit_id
left join DataImportTest.dbo.Entities c_local on c_local.feit_id = o_out.LocalContactId AND o_out.LocalContactId IS NOT NULL
left join DataImportTest.dbo.Entities c_primary on c_primary.feit_id = o_out.PrimaryContactId AND o_out.PrimaryContactId IS NOT NULL
RAISERROR('%d Organisations updated with primary / local contacts', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.[Organisation_CommunicationSettings]
([EntityID]	,[Medium],[CommType],[Frequency],[Subscribed],[LastUpdated])
SELECT e.Id	,[Medium],[CommType],[Frequency],[Subscribed],cs.[LastUpdated]
FROM CareerHub.dbo.[Organisation_CommunicationSettings] cs
INNER JOIN DataImportTest.dbo.Entities e on e.feit_id = cs.[EntityID]
RAISERROR('%d Organisation_CommunicationSettings', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.[OrganisationContact_CommunicationSettings]
([EntityID]	,[Medium],[CommType],[Frequency],[Subscribed],[LastUpdated])
SELECT e.Id	,[Medium],[CommType],[Frequency],[Subscribed],cs.[LastUpdated]
FROM CareerHub.dbo.[OrganisationContact_CommunicationSettings] cs
INNER JOIN DataImportTest.dbo.Entities e on e.feit_id = cs.[EntityID]
RAISERROR('%d OrganisationContact_CommunicationSettings', 10, 1, @@ROWCOUNT) WITH NOWAIT

insert DataImportTest.dbo.Individual_CommunicationSettings
([EntityID]	,[Medium],[CommType],[Frequency],[Subscribed],[LastUpdated])
SELECT e.Id	,[Medium],[CommType],[Frequency],[Subscribed],cs.[LastUpdated]
FROM CareerHub.dbo.Individual_CommunicationSettings cs
INNER JOIN DataImportTest.dbo.Entities e on e.feit_id = cs.[EntityID]
RAISERROR('%d Individual_CommunicationSettings', 10, 1, @@ROWCOUNT) WITH NOWAIT


-- jobs
INSERT DataImportTest.dbo.[PublishEntities] (
	[EntityId]	,[Title],[Summary],[IsActive],[IsWithdrawn],[Publish],[Expire])
SELECT e.Id		,[Title],[Summary],[IsActive],[IsWithdrawn],[Publish],[Expire]
FROM CareerHub.dbo.[PublishEntities] x
inner join DataImportTest.dbo.Entities e on e.feit_id = x.EntityId
RAISERROR('%d PublishEntities', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.[Jobs] (
	[EntityId],[EmployerId],[ContactId],[DisplayContactId],
	[RecruitingFor],[HideRecruitingFor],[Commences],[Remuneration],[PositionsAvailable],[ExternalReference],[Details],[Procedures],[MinimumAge],[Url] ,[IsGradRecruit],[IsVacEmployment],[IsWIL],[Residency],[Notifications_ExpireSent],[ContractType],[ContractHours])
SELECT
	e.id,emp.Id,c.Id,c_display.Id,
	[RecruitingFor],[HideRecruitingFor],[Commences],[Remuneration],[PositionsAvailable],[ExternalReference],[Details],[Procedures],[MinimumAge],[Url] ,[IsGradRecruit],[IsVacEmployment],[IsWIL],[Residency],[Notifications_ExpireSent],[ContractType],[ContractHours]
FROM CareerHub.dbo.[Jobs] x
inner join DataImportTest.dbo.Entities e on e.feit_id = x.EntityId
inner join DataImportTest.dbo.Entities emp on emp.feit_id = x.EmployerId
left join DataImportTest.dbo.Entities c on c.feit_id = x.ContactId AND x.ContactId IS NOT NULL
left join DataImportTest.dbo.Entities c_display on c_display.feit_id = x.DisplayContactId AND x.DisplayContactId IS NOT NULL
RAISERROR('%d Jobs', 10, 1, @@ROWCOUNT) WITH NOWAIT


-- forms
INSERT DataImportTest.dbo.[FormSubmissionStatuses] (
		[Name],[Colour],[Key],[AllowModifications])
SELECT  [Name],[Colour],[Key],[AllowModifications]
FROM CareerHub.dbo.[FormSubmissionStatuses]
WHERE Name NOT IN (SELECT Name FROM DataImportTest.dbo.[FormSubmissionStatuses])
RAISERROR('%d FormSubmissionStatuses', 10, 1, @@ROWCOUNT) WITH NOWAIT

-- targetting entities (ids are saved in delimited string)
declare @Id int, @Priority int, @TargetWorkGroups nvarchar(max)
declare c cursor for
	select e.Id, [Priority], TargetWorkGroups
	from CareerHub.dbo.TargettingEntities te
	inner join DataImportTest.dbo.Entities e on te.EntityId = e.feit_id
open c
fetch next from c into @Id, @Priority, @TargetWorkGroups
while @@FETCH_STATUS = 0 begin
	declare @result nvarchar(max) = ''

	select @result = @result + ',' + cast(wg.Id as nvarchar(10))
	from DataImportTest.dbo.Split(@TargetWorkGroups, '|') s
	inner join DataImportTest.dbo.WorkGroups wg on wg.feit_id = s.Item

	if(LEN(@result) > 0) set @result = right(@result, len(@result) - 1)
	else set @result = null

	insert DataImportTest.dbo.TargettingEntities (EntityId, [Priority], TargetWorkGroups)
	values (@Id, @Priority, @result)

	fetch next from c into @Id, @Priority, @TargetWorkGroups
end
close c
deallocate c


INSERT DataImportTest.dbo.[Forms] (
	[EntityId],[DefaultStatusID],
	[CompletionMsg],[CompletionUrl],[AllowRoles],[SingleSubmission],[IsPrivate],[IsAnonymous],[ExpireMessageTitle],[ExpireMessageBody])
SELECT
	e.Id,fss_in.Id,
	[CompletionMsg],[CompletionUrl],[AllowRoles],[SingleSubmission],[IsPrivate],[IsAnonymous],[ExpireMessageTitle],[ExpireMessageBody]
FROM CareerHub.dbo.[Forms] x
inner join DataImportTest.dbo.Entities e on e.feit_id = x.EntityId
inner join CareerHub.dbo.[FormSubmissionStatuses] fss_out on fss_out.id = x.DefaultStatusID
inner join DataImportTest.dbo.[FormSubmissionStatuses] fss_in on fss_in.name = fss_out.name
RAISERROR('%d Forms', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.[Forms_Pages] (
	feit_id,[FormId],[Title],[Description],[Index])
SELECT x.Id,f.Id	,[Title],[Description],[Index]
FROM CareerHub.dbo.[Forms_Pages] x
inner join DataImportTest.dbo.Entities f on f.feit_id = x.FormId
RAISERROR('%d Forms_Pages', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.[Forms_Fields] (
	feit_id,[PageId],[Type],[Label],  [Description],  [Index],[Properties],[IsActive],[FriendlyID])
SELECT x.Id,fp.Id	,[Type],[Label],x.[Description],x.[Index],[Properties],[IsActive],[FriendlyID]
FROM CareerHub.dbo.[Forms_Fields] x
inner join DataImportTest.dbo.[Forms_Pages] fp ON fp.feit_id = x.PageId
RAISERROR('%d Forms_Fields', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.[FormChangesets] ([FormID],[Date],[AdminID],[Changes])
SELECT f.Id,[Date],a.Id,[Changes]
FROM CareerHub.dbo.[FormChangesets] x
left join DataImportTest.dbo.Entities f on f.feit_id = x.FormID AND x.FormID IS NOT NULL
left join DataImportTest.dbo.Entities a on a.feit_id = x.AdminID AND x.AdminID IS NOT NULL
RAISERROR('%d FormChangesets', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.[FormEmailTemplates] (
	[FormID],[FromID]	,[Title],[Subject],[Body])
SELECT f.Id,est_in.Id	,[Title],[Subject],[Body]
FROM CareerHub.dbo.[FormEmailTemplates] x
inner join DataImportTest.dbo.Entities f on f.feit_id = x.FormID
inner JOIN CareerHub.dbo.Email_System_Addresses est_out on est_out.Id = x.[FromID]
inner JOIN DataImportTest.dbo.Email_System_Addresses est_in on ((
	est_in.[Type] in (0, 1) and est_out.[Type] = est_in.[Type]
) or (
	est_in.[Type] = 2 and est_out.Email = est_in.Email
))
RAISERROR('%d FormEmailTemplates', 10, 1, @@ROWCOUNT) WITH NOWAIT

-- questions
INSERT [CareerHub].dbo.[Questions] (
	[EntityId],[AdministratorID],[JobSeekerID],
	[Text],[Topic],[Closed],[AwaitingResponse],[LastViewed])
SELECT 
	e.Id,a.Id,js.Id,
	[Text],[Topic],[Closed],[AwaitingResponse],[LastViewed]
FROM [CareerHub_FEIT_Test].dbo.[Questions] x
inner join [CareerHub].dbo.Entities e on e.feit_id = x.EntityId
inner join [CareerHub].dbo.Entities js on js.feit_id = x.[JobSeekerID]
left join  [CareerHub].dbo.Entities a on a.feit_id = x.AdministratorID AND x.[AdministratorID] IS NOT NULL
RAISERROR('%d Questions', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT [CareerHub].dbo.[QuestionResponses] (feit_id,[QuestionID],[AdministratorID],[JobSeekerID],[Date],[Text])
SELECT x.[ID],q.Id,a.Id,js.Id,x.[Date],x.[Text]
FROM [CareerHub_FEIT_Test].dbo.[QuestionResponses] x
inner join [CareerHub].dbo.Entities js on js.feit_id = x.[JobSeekerID]
inner join [CareerHub].dbo.Entities q on q.feit_id = x.[QuestionID]
inner join [CareerHub].dbo.[Questions] chq on chq.EntityId = q.id 
left join  [CareerHub].dbo.Entities a on a.feit_id = x.[AdministratorID] AND x.[AdministratorID] IS NOT NULL
RAISERROR('%d QuestionResponses', 10, 1, @@ROWCOUNT) WITH NOWAIT

UPDATE q
SET DraftResponseID = qr.Id
FROM [CareerHub].dbo.[Questions] q
INNER JOIN [CareerHub].dbo.Entities e on q.EntityId = e.id 
INNER JOIN [CareerHub_FEIT_Test].dbo.[Questions] x ON e.feit_id = x.EntityId
INNER JOIN [CareerHub].dbo.[QuestionResponses] qr ON x.DraftResponseID = qr.feit_id
RAISERROR('%d Questions updated with DraftResponseID', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT [CareerHub].dbo.[QuestionUploads] ([QuestionID],[ResponseID],[UploadID],[StoredFileID])
SELECT q.Id,qr.Id,u.Id,sf.Id
FROM [CareerHub_FEIT_Test].dbo.[QuestionUploads] x
inner join [CareerHub].dbo.Entities q on q.feit_id = x.[QuestionID]
inner join [CareerHub].dbo.[Questions] chq on chq.EntityId = q.id 
LEFT JOIN [CareerHub].dbo.[QuestionResponses] qr ON x.[ResponseID] = qr.feit_id AND x.[ResponseID] IS NOT NULL
LEFT JOIN [CareerHub].dbo.StoredFiles sf ON x.[StoredFileID] = sf.feit_id AND x.[StoredFileID] IS NOT NULL
LEFT JOIN [CareerHub].dbo.uploads u ON x.[UploadID] = u.feit_id AND x.[UploadID] IS NOT NULL
RAISERROR('%d QuestionUploads', 10, 1, @@ROWCOUNT) WITH NOWAIT



-- Email Templates
INSERT DataImportTest.dbo.[CustomEmailTemplates] (
	[WorkgroupID],[From],[Title],[Subject],[Body])
SELECT wg.Id,est_in.Id,[Title],[Subject],[Body]
FROM CareerHub.dbo.[CustomEmailTemplates] x
LEFT JOIN DataImportTest.dbo.WorkGroups wg on wg.feit_id = x.WorkgroupID AND x.WorkgroupID IS NOT NULL
LEFT JOIN CareerHub.dbo.Email_System_Addresses est_out on est_out.Id = x.[From]
LEFT JOIN DataImportTest.dbo.Email_System_Addresses est_in on ((
	est_in.[Type] in (0, 1) and est_out.[Type] = est_in.[Type]
) or (
	est_in.[Type] = 2 and est_out.Email = est_in.Email
))
RAISERROR('%d CustomEmailTemplates', 10, 1, @@ROWCOUNT) WITH NOWAIT


-- Student Communication Settings
insert DataImportTest.dbo.JobSeeker_CommunicationSettings (
	EntityId,[Medium],[CommType],[Frequency],[Subscribed],  [LastUpdated])
SELECT js.Id,[Medium],[CommType],[Frequency],[Subscribed],x.[LastUpdated]
FROM CareerHub.dbo.JobSeeker_CommunicationSettings x
inner join DataImportTest.dbo.Entities js on js.feit_id = x.EntityId
where not exists (
	select null from DataImportTest.dbo.JobSeeker_CommunicationSettings jscs
	where jscs.EntityId = js.Id and jscs.Medium = x.Medium and jscs.CommType = x.CommType
)
RAISERROR('%d JobSeeker_CommunicationSettings', 10, 1, @@ROWCOUNT) WITH NOWAIT

-- Student Bookmarks
insert DataImportTest.dbo.Bookmarks_Jobs (ItemID, JobSeekerID, DateSaved, IsArchived)
select j.id, js.Id, x.DateSaved, x.IsArchived
FROM CareerHub.dbo.Bookmarks_Jobs x
inner join DataImportTest.dbo.Entities js on js.feit_id = x.JobSeekerID
inner join DataImportTest.dbo.Entities j on j.feit_id = x.ItemID
RAISERROR('%d Bookmarks_Jobs', 10, 1, @@ROWCOUNT) WITH NOWAIT

-- Students Following Organisations
insert DataImportTest.dbo.Follow_Organisations (OrganisationID, JobSeekerID, Added)
select o.id, js.Id, x.Added
FROM CareerHub.dbo.Follow_Organisations x
inner join DataImportTest.dbo.Entities js on js.feit_id = x.JobSeekerID
inner join DataImportTest.dbo.Entities o on o.feit_id = x.OrganisationID
RAISERROR('%d Follow_Organisations', 10, 1, @@ROWCOUNT) WITH NOWAIT

-- form submissions
INSERT DataImportTest.dbo.[Forms_Submissions] (
	feit_id,[FormId],[UserId],[EntityId],[UpdatedUserId],[StatusID],
	[Start],[Finish],[Updated],[Deleted])
SELECT
	x.[Id],f.Id,u.Id,e.Id,u_updated.Id,fss.Id,
	[Start],[Finish],[Updated],[Deleted]
FROM CareerHub.dbo.[Forms_Submissions] x
inner join CareerHub.dbo.FormSubmissionStatuses x_fss on x.StatusID = x_fss.ID
inner join DataImportTest.dbo.Entities f on f.feit_id = x.[FormId]
inner join DataImportTest.dbo.Entities e on e.feit_id = x.[EntityId]
left join DataImportTest.dbo.Users u on u.feit_id = x.[UserId]
left join DataImportTest.dbo.Users u_updated on u_updated.feit_id = x.[UpdatedUserId]
inner join DataImportTest.dbo.FormSubmissionStatuses fss on fss.Name = x_fss.Name
RAISERROR('%d Forms_Submissions', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.[Forms_Submissions_Values] ([SubmissionID],[FieldID],[Item],[Value])
SELECT s.Id,f.Id,[Item],[Value]
FROM CareerHub.dbo.[Forms_Submissions_Values] x
inner join DataImportTest.dbo.Forms_Fields f on f.feit_id = x.[FieldID]
inner join DataImportTest.dbo.Forms_Submissions s on s.feit_id = x.[SubmissionID]
RAISERROR('%d Forms_Submissions_Values', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.FormSubmissionFieldStates ([SubmissionID],[FieldID],[State])
SELECT s.Id,f.Id,[State]
FROM CareerHub.dbo.FormSubmissionFieldStates x
inner join DataImportTest.dbo.Forms_Fields f on f.feit_id = x.[FieldID]
inner join DataImportTest.dbo.Forms_Submissions s on s.feit_id = x.[SubmissionID]
RAISERROR('%d FormSubmissionFieldStates', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.FormSubmissionFileUploadTokens ([SubmissionID],[FieldID],Token,Added)
SELECT s.Id,f.Id,Token,Added
FROM CareerHub.dbo.FormSubmissionFileUploadTokens x
inner join DataImportTest.dbo.Forms_Fields f on f.feit_id = x.[FieldID]
inner join DataImportTest.dbo.Forms_Submissions s on s.feit_id = x.[SubmissionID]
RAISERROR('%d FormSubmissionFileUploadTokens', 10, 1, @@ROWCOUNT) WITH NOWAIT

-- form reports
INSERT DataImportTest.dbo.[FormReports]	(
	feit_id,[FormID],[UpdatedByAdminID],[AddedByAdminID],
    [Name],[IsPrivate],[AddedUTC],[UpdatedUTC],[SubmittedFromUTC],[SubmittedUntilUTC],[UpdatedFromUTC],[UpdatedUntilUTC],[ShowFriendlyIDs])
SELECT
	x.[ID],f.Id,a_updated.Id,a_added.Id,
    [Name],[IsPrivate],[AddedUTC],[UpdatedUTC],[SubmittedFromUTC],[SubmittedUntilUTC],[UpdatedFromUTC],[UpdatedUntilUTC],[ShowFriendlyIDs]
FROM CareerHub.dbo.[FormReports] x
inner join DataImportTest.dbo.Entities f on f.feit_id = x.[FormId]
inner join DataImportTest.dbo.Entities a_updated on a_updated.feit_id = x.[UpdatedByAdminID]
inner join DataImportTest.dbo.Entities a_added on a_added.feit_id = x.[AddedByAdminID]
RAISERROR('%d FormReports', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.FormReportColumns ([ReportID],[FieldID],[Item])
SELECT r.Id,f.Id,[Item]
FROM CareerHub.dbo.FormReportColumns x
inner join DataImportTest.dbo.[FormReports] r on r.feit_id = x.[ReportID]
inner join DataImportTest.dbo.Forms_Fields f on f.feit_id = x.[FieldID]
RAISERROR('%d FormReportColumns', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.FormReportOrderBys ([ReportID],[FieldID],[Item],Ascending)
SELECT r.Id,f.Id,[Item],Ascending
FROM CareerHub.dbo.FormReportOrderBys x
inner join DataImportTest.dbo.[FormReports] r on r.feit_id = x.[ReportID]
inner join DataImportTest.dbo.Forms_Fields f on f.feit_id = x.[FieldID]
RAISERROR('%d FormReportOrderBys', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.FormReportRunLog ([ReportID],FormID,AdministratorID,[Date])
SELECT r.Id,f.Id,a.Id,[Date]
FROM CareerHub.dbo.FormReportRunLog x
inner join DataImportTest.dbo.[FormReports] r on r.feit_id = x.[ReportID]
inner join DataImportTest.dbo.Entities f on f.feit_id = x.[FormId]
inner join DataImportTest.dbo.Entities a on a.feit_id = x.AdministratorID
RAISERROR('%d FormReportRunLog', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.[FormReportSearches] ([ReportID],[FieldID],[Item],[Value],[Operation])
SELECT r.Id,f.Id,[Item],[Value],[Operation]
FROM CareerHub.dbo.[FormReportSearches] x
inner join DataImportTest.dbo.[FormReports] r on r.feit_id = x.[ReportID]
inner join DataImportTest.dbo.Forms_Fields f on f.feit_id = x.[FieldID]
RAISERROR('%d FormReportSearches', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.FormReportStatuses ([ReportID],StatusID)
SELECT r.Id, fss.Id
FROM CareerHub.dbo.FormReportStatuses x
inner join CareerHub.dbo.FormSubmissionStatuses x_fss on x.StatusID = x_fss.ID
inner join DataImportTest.dbo.[FormReports] r on r.feit_id = x.[ReportID]
inner join DataImportTest.dbo.FormSubmissionStatuses fss on fss.Name = x_fss.Name
RAISERROR('%d FormReportStatuses', 10, 1, @@ROWCOUNT) WITH NOWAIT


-- Sent Emails
INSERT DataImportTest.dbo.[Email_Batches] (
	feit_id,[SentByAdminID],
	[DisplayName],[EmailType],[RecipientType],[CommType],[SendToUnconfirmed],[From],[TemplateKey],[SubjectTemplate],[BodyTemplate],[Created],[IsPreparedForSend],[StartSendAfter],[SendStarted],[SendRestarted],[SendFinished],[ErrorType],[ErrorSummary],[ErrorDetail],[IsLegacyRecord],[IsCancelled])
SELECT
	x.[ID],a.Id,
	[DisplayName],[EmailType],[RecipientType],[CommType],[SendToUnconfirmed],[From],[TemplateKey],[SubjectTemplate],[BodyTemplate],[Created],[IsPreparedForSend],[StartSendAfter],[SendStarted],[SendRestarted],[SendFinished],[ErrorType],[ErrorSummary],[ErrorDetail],[IsLegacyRecord],[IsCancelled]
FROM CareerHub.dbo.[Email_Batches] x
left join DataImportTest.dbo.Entities a on a.feit_id = x.[SentByAdminID] and x.[SentByAdminID] is not null
RAISERROR('%d Email_Batches', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.[Email_Batch_Attempts] (
	[BatchID],[Started],[Finished],  [ErrorType],  [ErrorDetail])
SELECT b.Id  ,[Started],[Finished],x.[ErrorType],x.[ErrorDetail]
FROM CareerHub.dbo.[Email_Batch_Attempts] x
INNER JOIN DataImportTest.dbo.[Email_Batches] b on b.feit_id = x.BatchID
RAISERROR('%d Email_Batch_Attempts', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.[Email_Jobs] (
	feit_id,[BatchID],[EntityID],[WorkGroupID],
	[EntityType],[To],[CC],[BCC],  [From],[Subject],[Body],[Token],[Started],[Finished],[Attempts],  [ErrorType],  [ErrorSummary],  [ErrorDetail])
SELECT
	x.[ID],b.id,e.id,wg.id,
	[EntityType],[To],[CC],[BCC],x.[From],[Subject],[Body],[Token],[Started],[Finished],[Attempts],x.[ErrorType],x.[ErrorSummary],x.[ErrorDetail]
FROM CareerHub.dbo.[Email_Jobs] x
INNER JOIN DataImportTest.dbo.[Email_Batches] b on b.feit_id = x.BatchID
LEFT JOIN DataImportTest.dbo.WorkGroups wg on wg.feit_id = x.[WorkGroupID]
LEFT JOIN DataImportTest.dbo.Entities e on e.feit_id = x.[EntityID]
RAISERROR('%d Email_Jobs', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.Email_Job_Attachments (JobId,FilePath)
SELECT j.Id,FilePath
FROM CareerHub.dbo.Email_Job_Attachments x
INNER JOIN DataImportTest.dbo.[Email_Jobs] j on j.feit_id = x.JobID
RAISERROR('%d Email_Job_Attachments', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.[Email_Mailouts] (
	feit_id,[BatchID],[FromAdminID],[FromSystemEmailID],[ApprovalAdminID],[WorkGroupID],[ApprovedAdminID],
	  [CommType],  [RecipientType],[FromEmail],[FromEmailConfirmed],[FromEmailDeactivated],[UseLayout],[Subject],[Body],  [Signature],[PrepareStarted],[PrepareRestarted],[PrepareFinished],  [Created],  [LastUpdated],[IsApproved],[ApprovalDate],  [IsSoftDeleted])
SELECT
	x.[ID],b.id,a_from.id,est_in.Id,a_approval.id,wg.Id,a_approved.id,
	x.[CommType],x.[RecipientType],[FromEmail],[FromEmailConfirmed],[FromEmailDeactivated],[UseLayout],[Subject],[Body],x.[Signature],[PrepareStarted],[PrepareRestarted],[PrepareFinished],x.[Created],x.[LastUpdated],[IsApproved],[ApprovalDate],x.[IsSoftDeleted]
FROM CareerHub.dbo.[Email_Mailouts] x
INNER JOIN DataImportTest.dbo.[Email_Batches] b on b.feit_id = x.BatchID
INNER JOIN DataImportTest.dbo.Entities a_from on a_from.feit_id = x.FromAdminID
LEFT JOIN DataImportTest.dbo.Entities a_approval on a_approval.feit_id = x.ApprovalAdminID
LEFT JOIN DataImportTest.dbo.Entities a_approved on a_approved.feit_id = x.ApprovedAdminID
LEFT JOIN DataImportTest.dbo.WorkGroups wg on wg.feit_id = x.WorkGroupID
LEFT JOIN CareerHub.dbo.Email_System_Addresses est_out on est_out.Id = x.FromSystemEmailID
LEFT JOIN DataImportTest.dbo.Email_System_Addresses est_in on ((
	est_in.[Type] in (0, 1) and est_out.[Type] = est_in.[Type]
) or (
	est_in.[Type] = 2 and est_out.Email = est_in.Email
))
RAISERROR('%d Email_Mailouts', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.Email_MailoutEntityRecipients (MailoutID,EntityID)
SELECT m.Id,e.Id
FROM CareerHub.dbo.Email_MailoutEntityRecipients x
INNER JOIN DataImportTest.dbo.Entities e on e.feit_id = x.EntityID
INNER JOIN DataImportTest.dbo.[Email_Mailouts] m on m.feit_id = x.MailoutID
RAISERROR('%d Email_MailoutEntityRecipients', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.Email_Mailout_CopyToEntity (MailoutID,EntityID)
SELECT m.Id,e.Id
FROM CareerHub.dbo.Email_Mailout_CopyToEntity x
INNER JOIN DataImportTest.dbo.Entities e on e.feit_id = x.EntityID
INNER JOIN DataImportTest.dbo.[Email_Mailouts] m on m.feit_id = x.MailoutID
RAISERROR('%d Email_Mailout_CopyToEntity', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.Email_Mailout_CopyToEmail (MailoutID,Email)
SELECT m.Id,[Email]
FROM CareerHub.dbo.Email_Mailout_CopyToEmail x
INNER JOIN DataImportTest.dbo.[Email_Mailouts] m on m.feit_id = x.MailoutID
RAISERROR('%d Email_Mailout_CopyToEmail', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.[EmailUnsubscribeLog] (
	[EntityID]	,[EntityTypeName],[CommType],[Reason],[LogID],[LogType])
SELECT e.id		,[EntityTypeName],[CommType],[Reason],[LogID],[LogType]	
FROM CareerHub.dbo.[EmailUnsubscribeLog] x
INNER JOIN DataImportTest.dbo.Entities e on e.feit_id = x.EntityID
RAISERROR('%d EmailUnsubscribeLog', 10, 1, @@ROWCOUNT) WITH NOWAIT

-- convert industries to labels
INSERT DataImportTest.dbo.Entities_LabelDefinitions ([Type],[Label],[Created],[LastEdited],[IsLocked],[IsPrivate],[IsArchived])
SELECT 0, 'Industry - ' + c.Name, GETUTCDATE(), GETUTCDATE(), 0, 0, 0
FROM CareerHub.dbo.Categories c
INNER JOIN CareerHub.dbo.CategoryKeys ck on c.ParentId = ck.CategoryId
WHERE ck.[Key] = 'industry'
RAISERROR('%d Industry Entities_LabelDefinitions', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.Entities_Labels (EntityId, LabelId)
SELECT e.Id, eld.Id
FROM CareerHub.dbo.AppliedCategories ac
INNER JOIN CareerHub.dbo.Categories c ON ac.CategoryId = c.Id
INNER JOIN CareerHub.dbo.CategoryKeys ck on c.ParentId = ck.CategoryId
INNER JOIN DataImportTest.dbo.Entities e on e.feit_id = ac.EntityID
INNER JOIN DataImportTest.dbo.Entities_LabelDefinitions eld on eld.Label = 'Industry - ' + c.Name
RAISERROR('%d Industry Entities_Labels', 10, 1, @@ROWCOUNT) WITH NOWAIT

-- add "FEIT Import" label
DECLARE @labelId INT, @journalLabelId INT

INSERT DataImportTest.dbo.Entities_LabelDefinitions (Label, [Type], Created, LastEdited) 
VALUES ('FEIT Import', 0, GETUTCDATE(), GETUTCDATE())
SET @labelid = @@IDENTITY

INSERT DataImportTest.dbo.Entities_LabelDefinitions (Label, [Type], Created, LastEdited) 
VALUES ('FEIT Import', 1, GETUTCDATE(), GETUTCDATE())
SET @journalLabelId = @@IDENTITY

INSERT DataImportTest.dbo.Entities_Labels (EntityId, LabelId)
SELECT Id, CASE TypeName WHEN 'JournalEntry' THEN @journalLabelId ELSE @labelId END
FROM DataImportTest.dbo.Entities
WHERE feit_id IS NOT NULL AND (
	TypeName <> 'JobSeeker' 
	OR ID in (
		select entityId from DataImportTest.dbo.jobSeekers 
		where IsProvisioned = 0 AND ExternalId not in (
			select ExternalId from DataImportTest.dbo.JobSeekers
		)
	)
)
RAISERROR('%d ''FEIT Import'' Entities_Labels', 10, 1, @@ROWCOUNT) WITH NOWAIT

-- add "FEIT ID" extended property
INSERT DataImportTest.dbo.Entities_PropertyDefinitions ([EntityTypeName],[PropertyName],[PropertyType],[IsAdminOnly],[DisplayName])
SELECT TypeName,'FEIT ID','Int',1,'FEIT ID'
FROM DataImportTest.dbo.Entities
WHERE feit_id IS NOT NULL
GROUP BY TypeName
RAISERROR('%d ''FEIT ID'' Entities_PropertyDefinitions', 10, 1, @@ROWCOUNT) WITH NOWAIT

INSERT DataImportTest.dbo.Entities_Properties ([EntityId],[PropertyName],[PropertyValue])
SELECT Id,'FEIT ID',CAST(feit_id AS NVARCHAR(10))
FROM DataImportTest.dbo.Entities
WHERE feit_id IS NOT NULL
RAISERROR('%d ''FEIT ID'' Entities_Properties', 10, 1, @@ROWCOUNT) WITH NOWAIT

-- get data for file imports
SELECT 'Attachments' AS 'Src', 'Attachments' AS 'Dest'
UNION ALL
SELECT 'EmailAttachments', 'EmailAttachments'
UNION ALL
SELECT 'EmailHeaders', 'EmailHeaders'
UNION ALL
SELECT 'Images', 'Images'
UNION ALL
SELECT
	'questions\' + cast(feit_id as nvarchar(10)),
	'questions\' + cast(id as nvarchar(10))
FROM DataImportTest.dbo.Entities e
WHERE typename = 'Question'
AND e.feit_id IS NOT NULL
UNION ALL
SELECT 
	'formBuilder\' + cast(e.feit_id as nvarchar(10)) + '\' + cast(fs.feit_id as nvarchar(10)),
	'formBuilder\' + cast(formId as nvarchar(10)) + '\' + cast(fs.Id as nvarchar(10))
FROM DataImportTest.dbo.Forms_Submissions fs
INNER JOIN DataImportTest.dbo.Entities e on e.Id = fs.FormId
WHERE e.feit_id IS NOT NULL
Cleanup:
go

DROP INDEX ux_Entities_feit_id ON DataImportTest.dbo.Entities
DROP INDEX ux_Users_feit_id ON DataImportTest.dbo.Users
DROP INDEX ux_ContactDetails_feit_id ON DataImportTest.dbo.ContactDetails
DROP INDEX ux_Administrator_EmailAddresses_feit_id ON DataImportTest.dbo.Administrator_EmailAddresses
DROP INDEX ux_Organisation_EmailAddresses_feit_id ON DataImportTest.dbo.Organisation_EmailAddresses
DROP INDEX ux_Individual_EmailAddresses_feit_id ON DataImportTest.dbo.Individual_EmailAddresses
DROP INDEX ux_OrganisationContact_EmailAddresses_feit_id ON DataImportTest.dbo.OrganisationContact_EmailAddresses
DROP INDEX ux_JobSeeker_EmailAddresses_feit_id ON DataImportTest.dbo.JobSeeker_EmailAddresses
DROP INDEX ux_SubSites_feit_id ON DataImportTest.dbo.SubSites
DROP INDEX ux_Blogs_feit_id ON DataImportTest.dbo.Blogs
DROP INDEX ux_Workgroups_feit_id ON DataImportTest.dbo.Workgroups
DROP INDEX ux_StoredFiles_feit_id ON DataImportTest.dbo.StoredFiles
DROP INDEX ux_Uploads_feit_id ON DataImportTest.dbo.Uploads
DROP INDEX ux_Forms_Pages_feit_id ON DataImportTest.dbo.Forms_Pages
DROP INDEX ux_Forms_Fields_feit_id ON DataImportTest.dbo.Forms_Fields
DROP INDEX ux_FormReports_feit_id ON DataImportTest.dbo.FormReports
DROP INDEX ux_Forms_Submissions_feit_id ON DataImportTest.dbo.Forms_Submissions
DROP INDEX ux_QuestionResponses_feit_id ON DataImportTest.dbo.QuestionResponses
DROP INDEX ux_Email_Batches_feit_id ON DataImportTest.dbo.Email_Batches
DROP INDEX ux_Email_Jobs_feit_id ON DataImportTest.dbo.Email_Jobs
DROP INDEX ux_Email_Mailouts_feit_id ON DataImportTest.dbo.Email_Mailouts
go

alter table DataImportTest.dbo.Entities drop column feit_id;
alter table DataImportTest.dbo.Users drop column feit_id;
alter table DataImportTest.dbo.ContactDetails drop column feit_id;
alter table DataImportTest.dbo.Administrator_EmailAddresses drop column feit_id;
alter table DataImportTest.dbo.Organisation_EmailAddresses drop column feit_id;
alter table DataImportTest.dbo.Individual_EmailAddresses drop column feit_id;
alter table DataImportTest.dbo.OrganisationContact_EmailAddresses drop column feit_id;
alter table DataImportTest.dbo.JobSeeker_EmailAddresses drop column feit_id;
alter table DataImportTest.dbo.SubSites drop column feit_id;
alter table DataImportTest.dbo.Blogs drop column feit_id;
alter table DataImportTest.dbo.Workgroups drop column feit_id;
alter table DataImportTest.dbo.StoredFiles drop column feit_id;
alter table DataImportTest.dbo.Uploads drop column feit_id;
alter table DataImportTest.dbo.Forms_Pages drop column feit_id;
alter table DataImportTest.dbo.Forms_Fields drop column feit_id;
alter table DataImportTest.dbo.FormReports drop column feit_id;
alter table DataImportTest.dbo.Forms_Submissions drop column feit_id;
alter table DataImportTest.dbo.QuestionResponses drop column feit_id;
alter table DataImportTest.dbo.Email_Batches drop column feit_id;
alter table DataImportTest.dbo.Email_Jobs drop column feit_id;
alter table DataImportTest.dbo.Email_Mailouts drop column feit_id;
go

rollback transaction

